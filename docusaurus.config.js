module.exports = {
  title: 'Gosense',
  tagline: '轻便灵活的博客系统',
  url: 'https://www.netroby.com',
  baseUrl: '/gosense/',
  onBrokenLinks: 'throw',
  favicon: 'img/favicon.ico',
  organizationName: '', // Usually your GitHub org/user name.
  projectName: '', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Gosense',
      logo: {
        alt: '博客系统',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: '文档',
          position: 'left',
        },
        {
          href: 'https://gitee.com/cnmade/gosense',
          label: '源代码',
          position: 'left',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: '文档',
          items: [
            {
              label: '快速开始',
              to: 'docs/',
            },
            {
              label: '文档',
              to: 'docs/doc2/',
            },
          ],
        },
        {
          title: '社区',
          items: [
            {
              label: '官网',
              href: 'https://www.netroby.com',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/netroby',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: '源代码',
              href: 'https://gitee.com/cnmade/gosense',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} CnMade. `,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://www.netroby.com/#',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://www.netroby.com/#',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
